#!/bin/sh
docker build -t APP001 . --no-cache
docker tag APP001:latest URL-TO-DOCKER-REGISTRY/APP001
docker push URL-TO-DOCKER-REGISTRY/APP001
cloudron install --image URL-TO-DOCKER-REGISTRY/APP001
