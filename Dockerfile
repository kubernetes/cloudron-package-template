FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

ARG VERSION=0.21

RUN apt update && apt install openssl

RUN mkdir -p /app/code
WORKDIR /app/code

# EITHER 

ADD https://REPOURL/v${VERSION}/PACKAGE /app/code

# OR ALTERNATIVE

RUN curl -L https://REPOURL/${VERSION}.zip -o /tmp/tmp.zip && unzip /tmp/tmp.zip -d /tmp && rm /tmp/tmp.zip
RUN mv /tmp/FOLDERNAME-${VERSION}/* /app/code/

# ALTERNATIVE END

COPY start.sh /app/code/

#CMD [ "/app/code/start.sh" ]
