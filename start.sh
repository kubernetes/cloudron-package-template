#!/bin/bash

set -eu

chown -R cloudron:cloudron /app/data

echo "==> Starting App"
exec /usr/local/bin/gosu cloudron:cloudron BINARY
