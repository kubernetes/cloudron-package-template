# cloudron-app-package
Package for deloyment of this app on Cloudron

## THIS IS ONLY A PROJECT TO MAKE IT POSSIBLE TO DEPLOY THIS APP ON CLOUDRON 

@rosano wrote a nice [guide how to install a custom app via private registry and cloudron build](https://rosano.ca/log/01hs9tx1ytkp3kb0v03pdpm08a)

(a copy of the guide is following with permission of Rosano)

> > Finally managed to install a custom app via Cloudron by following the [tutorial documentation](https://docs.cloudron.io/packaging/tutorial/), and without installing Docker.
> > 
> > ### 1\. Install the Cloudron CLI locally
> > 
> >     sudo npm install -g cloudron
> >     cloudron login my.example.com
> >     
> > 
> > ### 2\. Setup build tools
> > 
> > One-click install the [Docker Registry App](https://docs.cloudron.io/apps/docker-registry/) (replace `alfa.bravo` below with this app domain) and [Build Service App](https://docs.cloudron.io/apps/build-service/) (replace `charlie.delta` below with this app domain) via your Cloudron App Store. Configure the latter’s credentials in `/app/data/docker.json`
> > 
> >     {
> >       "alfa.bravo": {
> >         "username": "CLOUDRON_USERNAME",
> >         "password": "CLOUDRON_PASSWORD"
> >       }
> >     }
> >     
> > 
> > and restart the app.
> > 
> > ### 3\. Install the pre-packaged custom app
> > 
> > Replace the [simple tutorial app](https://git.cloudron.io/cloudron/tutorial-nodejs-app) with yours:
> > 
> >     git clone https://git.cloudron.io/cloudron/tutorial-nodejs-app
> >     cd tutorial-nodejs-app
> >     cloudron build --set-build-service https://charlie.delta --set-repository alfa.bravo/echo/foxtrot --tag golf
> >     cloudron install --image alfa.bravo/echo/foxtrot:golf
> >     
> > 
> > If you want to update, run `cloudron build` again, then call `cloudron update` like so:
> > 
> >     cloudron build --set-build-service https://charlie.delta --set-repository alfa.bravo/echo/foxtrot --tag golf
> >     cloudron update --image alfa.bravo/echo/foxtrot:golf
> >     
> > 
> > #### Resources
> > 
> > Aside from the [tutorial documentation](https://docs.cloudron.io/packaging/tutorial/), I found some hints in these forum topics:
> > 
> > -   [How to build (custom) apps using the docker-registry](https://forum.cloudron.io/topic/4412/how-to-build-custom-apps-using-the-docker-registry)
> > -   [How do I do this??](https://forum.cloudron.io/topic/6717/how-do-i-do-this)
> > -   [UI not working](https://forum.cloudron.io/topic/9957/ui-not-working)



# An alternative to the recommended way to install you can also use the Easy Installation
Be aware that you will use a precreated Docker Image this way - you have to trust the Image Provider if you use this way to install

You will need to have a *read and write* Cloudron API Token. See [Documentation how to create one](https://docs.cloudron.io/profile/#api-tokens).

1) Install a new temporary Surfer App on Cloudron
2) Open Terminal of Surfer App
3) Enter ```cd /app/data```
4) Enter ```wget [URL TO THE RELEASE ZIP OF THE GIT REPO]```
5) Enter ```unzip [FILENAME OF THE DOWNLOADED ZIP] && cd [NAME OF THE UNZIPPED FOLDER]```
6) Edit the config.ini with File Editor in File Manager and enter the Cloudron API Acess Token, the Cloudron Instance Hostname and the desired App Location (Subdomain)
7) Go back to the Terminal and enter ```./easy-install.sh```
8) The installation take a couple of minutes and will tell you if it was successfull
9) The temporary Surfer App can be uninstalled now as it is not needed for running the App. If you reused and existing Surfer App, you can skip this step.
10) The installed package will now be visible in the Cloudron App panel. If it shows your Cloudron login, you can login with the cloudron credentials.

**Important: Please do not download the Archive to any other place than /app/data. If you put it in public/ you risk leakage of the Cloudron API Token you put in the config.ini file!**

**Limitations:**
- A App installed with easy-installation cannot be relocated with native Cloudron configuration. You will need to perform an easy install for the new location instead.

# Warning: Not Production Ready

Please note that this project is not intended for production use. It is only a project to make it possible to deploy this app on Cloudron. Deploying this app manually on Cloudron requires advanced knowledge of Linux, the Cloudron CLI, and Docker.

Using a precreated Docker Image from an external source for easy installation comes with risks. You need to trust the Image Provider if you choose this installation method.

Before proceeding with the installation, please ensure that you understand the potential risks involved and take appropriate precautions to protect your system and data.

By continuing with the installation, you acknowledge and accept full responsibility for any consequences that may arise from using this project.

## License
This project is under the [GNU GPLv3](LICENSE).

